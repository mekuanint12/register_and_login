package valid

import (
	"errors"
	"net/mail"
	"regexp"
	"unicode"

	_ "github.com/lib/pq"
	betterPassword "github.com/wagslane/go-password-validator"
)

func ValidateUsername(username string) error {
	for _, char := range username {
		if !unicode.IsLetter(char) && !unicode.IsNumber(char) {
			return errors.New("only alphanumeric characters allowed")
		}
	}
	if len(username) <= 5 && len(username) > 15 {
		return errors.New("username too short")
	}
	return nil
}

func ValidateField(val string) error {
	if len(val) == 0 {
		return errors.New("empty field")
	} else if len(val) < 4 {
		return errors.New("too Short")
	}
	return nil
}

func ValidatePassword(pass string) error {
	err := betterPassword.Validate(pass, 40)
	if err != nil {
		return errors.New("weak password")
	}
	return nil
}

func ValidateEmail(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

func IsEmailValid(e string) bool {
	emailRegex := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	return emailRegex.MatchString(e)
}
