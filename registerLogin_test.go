package main

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/cucumber/godog"
	api "github.com/mekuanint12/simple_api/api"
	db "github.com/mekuanint12/simple_api/db/sqlc"
	"github.com/mekuanint12/simple_api/util"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"

	_ "github.com/lib/pq"
)

func testServer() *api.Server {
	config, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("Cant load config file: ", err)
	}
	con, err := sql.Open(config.DBDriver, config.DBSource)
	if err != nil {
		log.Fatal("Can not connect to the database:", err)
	}
	create_account := db.NewUser(con)
	server := api.NewServer(create_account)
	return server
}

type TestX struct {
	suite    *godog.ScenarioContext
	response string
}

var nonAlphanumericRegex = regexp.MustCompile(`[^a-zA-Z0-9 ]+`)

func clearString(str string) string {
	return nonAlphanumericRegex.ReplaceAllString(str, "")
}

func thereIsANewUser() error {
	return nil
}

func thereIsAnExistingUser() error {
	return nil
}

func (ts *TestX) userCreatesAnAccountByInsertingAndAndAndAnd(username, firstname, lastname, pass, email string) error {
	arg := db.CreateAccountParams{
		UserName:  username,
		FirstName: firstname,
		LastName:  lastname,
		Password:  pass,
		Email:     email,
	}
	body, err := json.Marshal(arg)
	if err != nil {
		log.Fatal(err)
	}
	req, _ := http.NewRequest(http.MethodPost, "/register", bytes.NewBufferString(string(body)))
	recorder := httptest.NewRecorder()
	server := testServer()
	server.Router.ServeHTTP(recorder, req)
	messge, _ := ioutil.ReadAll(recorder.Body)
	s := strings.Split(string(messge), ":")
	ts.response = clearString(s[1])

	return nil
}
func (ts *TestX) userLogsIntoAccountByInsertingAnd(usernames, password string) error {
	arg := db.GetAccountParams{
		UserName: usernames,
		Password: password,
	}
	body, err := json.Marshal(arg)
	if err != nil {
		log.Fatal(err)
	}
	req, _ := http.NewRequest(http.MethodGet, "/login", bytes.NewBufferString(string(body)))
	recorder := httptest.NewRecorder()
	server := testServer()
	server.Router.ServeHTTP(recorder, req)
	// fmt.Println(recorder.Body.String())
	messge, _ := ioutil.ReadAll(recorder.Body)
	s := strings.Split(string(messge), ":")
	ts.response = clearString(s[1])

	return nil
}

func (ts *TestX) theResultShouldBe(expected string) error {
	if ts.response != expected {
		return fmt.Errorf("expected %s, got %s", expected, ts.response)
	}
	return nil
}
func InitializeScenario(ctx *godog.ScenarioContext) {
	db := TestX{
		suite: ctx,
	}
	ctx.Step(`^the result should be "([^"]*)"$`, db.theResultShouldBe)
	ctx.Step(`^there is a new user$`, thereIsANewUser)
	ctx.Step(`^User creates an account by inserting "([^"]*)" and "([^"]*)" and "([^"]*)"  and "([^"]*)" and "([^"]*)"$`, db.userCreatesAnAccountByInsertingAndAndAndAnd)
	ctx.Step(`^User creates an account by inserting "([^"]*)" and "([^"]*)" and "([^"]*)"  and "([^"]*)""" and "([^"]*)"$`, db.userCreatesAnAccountByInsertingAndAndAndAnd)
	ctx.Step(`^User creates an account by inserting "([^"]*)" and "([^"]*)""" and "([^"]*)"  and "([^"]*)" and "([^"]*)"$`, db.userCreatesAnAccountByInsertingAndAndAndAnd)
	ctx.Step(`^there is an existing user$`, thereIsAnExistingUser)
	ctx.Step(`^User logs into account by inserting "([^"]*)" and "([^"]*)"$`, db.userLogsIntoAccountByInsertingAnd)
}
