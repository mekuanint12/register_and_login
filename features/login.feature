@alluserflow
Feature: User Login 


  Background: 
    Given there is an existing user

  @login
  Scenario: Login Functionality
    When User logs into account by inserting "<username>" and "<password>"
    Then the result should be "<expected result>"
    Examples: logging in into an acoount
    | username   |    password   |   expected result  |
    |   Meku1    | #Za34*meku6   |     Welcome Back  |
    |   Meku2    | #Za34*meku88  |     Welcome Back  | 

  @login
  Scenario: User provide empty password or username
    When User logs into account by inserting "<username>" and "<password>"
    Then the result should be "<expected result>"
    Examples: logging in with no password
    | username   |    password      |  expected result     |
    |   Meku1    |                  |    empty field       |
    |            |  #Za34*meku88    |    empty field       | 

  @login
  Scenario: Unsuccessful login with wrong Username or Password
    When User logs into account by inserting "<username>" and "<password>"
    Then the result should be "<expected result>"
    Examples: logging in into an acoount
    | username|    password        |          expected result           |
    |   Meku1    |    abced11      | check username and password        |

