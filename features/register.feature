@alluserflow
Feature: User Registration

  Background: 
    Given there is a new user

  @registration
  Scenario: Creating a new account
    When User creates an account by inserting "<username>" and "<firstname>" and "<lastname>"  and "<password>" and "<email>"
    Then the result should be "<expected result>"
    Examples: creating new account
    | username   | firstname    | lastname   |  password      |       email             |         expected result     |
    |   Meku1    |  MekuLa      |   last     | #Za34*meku6    |     meku@meku.com       |      Welcome                |
    |   Meku2    |  Mekuanit    |   Legese   | #Za34*meku88   |     def@gmail.com       |       Welcome               |
  
  @registration
  Scenario: User doesn't provide a valid data
    When User creates an account by inserting "<username>" and "<firstname>" and "<lastname>"  and "<password>" and "<email>"
    Then the result should be "<expected result>"
    Examples: validation errors
    | username   | firstname    | lastname   |   password     |       email             |     expected result   |
    |   Meku1    |  Meku        |            |   #Za34*meku88 |     meku@meku.com       |   empty field         |
    |   Meku2    |              |   Legese   | #Za34*meku88   |     def@gmail.com       |   empty field       |
    |   Dani1    |  Dani        |   last     |   1234567      |     meku@mek.com        |   weak password       |
    |   Selomn2  |  selmon      |   abebe    | #Za34*meku88   |       myemail@          |   Email Invalid      |

  # @registration
  # Scenario: User inserted an existing Username
  #   When User creates an account by inserting "<username>" and "<firstname>" and "<lastname>"  and "<password>" and "<email>"
  #   When we already have an existing "<usernames>"
  #     |<usernames> |   password     |
  #     | meku1      |     #Za34*meku6   |
  #     | Dani2      |     user123       |
  #   Then the result should be "<expected result>"
  #   Examples: existing user name error
  #   | username | firstname  | lastname|  password |       email         |     expected result |
  #   |   Meku1    |  Meku        |   last     |   abcdef1    |     meku@meku.com       |     "Username Taken"    |
  #   |   Meku2    |  Mekuanit    |   Legese   |   xyz123     |     def@gmail.com       |      "Username Taken"   |