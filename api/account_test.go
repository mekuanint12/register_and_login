package api

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	_ "github.com/lib/pq"
)

type errorsMessage struct {
	Message string `json:"message"`
	Error   string `json:"error"`
}

func TestRegisterNewUser(t *testing.T) {
	t.Run("creating new account as a new user", func(t *testing.T) {
		arg := CreateAccountRequest{
			UserName:  "meku23",
			FirstName: "meku",
			LastName:  "laster",
			Password:  "meku*#76&Ma1Mek9",
			Email:     "teste21@tests.com",
		}
		body, err := json.Marshal(arg)
		if err != nil {
			t.Fatal(err)
		}
		req, _ := http.NewRequest(http.MethodPost, "/register", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)
		var msg errorsMessage
		messge, _ := ioutil.ReadAll(recorder.Body)
		json.Unmarshal(messge, &msg)
		if status := recorder.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: %v", recorder.Code)
			return
		}

		assertError(t, msg.Message, "Welcome")
	})
	t.Run("User inserting Weak Password", func(t *testing.T) {
		arg := CreateAccountRequest{
			UserName:  "meku01",
			FirstName: "meku",
			LastName:  "laster",
			Password:  "12345678",
			Email:     "teste01@tests.com",
		}
		body, err := json.Marshal(arg)
		if err != nil {
			t.Fatal(err)
		}
		req, _ := http.NewRequest(http.MethodPost, "/register", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)
		var ereMsg errorsMessage
		messge, _ := ioutil.ReadAll(recorder.Body)
		json.Unmarshal(messge, &ereMsg)
		assertError(t, ereMsg.Error, "weak password")
	})
	t.Run("User inserting Empty field", func(t *testing.T) {
		arg := CreateAccountRequest{
			UserName:  "meku01",
			FirstName: "",
			LastName:  "laster",
			Password:  "12345678",
			Email:     "teste01@tests.com",
		}
		body, err := json.Marshal(arg)
		if err != nil {
			t.Fatal(err)
		}
		req, _ := http.NewRequest(http.MethodPost, "/register", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)
		var ereMsg errorsMessage
		messge, _ := ioutil.ReadAll(recorder.Body)
		json.Unmarshal(messge, &ereMsg)
		assertError(t, ereMsg.Error, "empty field")
	})

	t.Run("Invalid Email", func(t *testing.T) {
		arg := CreateAccountRequest{
			UserName:  "meku03",
			FirstName: "meku",
			LastName:  "laster",
			Password:  "1#2Dac&we4M",
			Email:     "myemail",
		}
		body, err := json.Marshal(arg)
		if err != nil {
			t.Fatal(err)
		}

		req, _ := http.NewRequest(http.MethodPost, "/register", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)
		var ereMsg errorsMessage
		messge, _ := ioutil.ReadAll(recorder.Body)
		json.Unmarshal(messge, &ereMsg)
		assertError(t, ereMsg.Message, "Email Invalid")
	})

}

func TestLoginFeature(t *testing.T) {
	t.Run("Logging in as an existing user", func(t *testing.T) {
		arg := GetAccountParams{
			UserName: "meku23",
			Password: "meku*#76&Ma1Mek9",
		}
		body, err := json.Marshal(arg)
		if err != nil {
			t.Fatal(err)
		}
		req, _ := http.NewRequest(http.MethodGet, "/login", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)
		// t.Fatal(recorder.Body.String())
		if status := recorder.Code; status != http.StatusOK {
			t.Errorf("handler returned wrong status code: %v", recorder.Code)
			return
		}
		var ereMsg errorsMessage
		messge, _ := ioutil.ReadAll(recorder.Body)
		json.Unmarshal(messge, &ereMsg)

		assertError(t, ereMsg.Message, "Welcome Back!")
	})

	t.Run("Logging in wrong password", func(t *testing.T) {
		arg := GetAccountParams{
			UserName: "meku23",
			Password: "*#meku7777",
		}
		body, err := json.Marshal(arg)
		if err != nil {
			t.Fatal(err)
		}
		req, _ := http.NewRequest(http.MethodGet, "/login", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)
		// t.Fatal(recorder.Body.String())
		var ereMsg errorsMessage
		messge, _ := ioutil.ReadAll(recorder.Body)
		json.Unmarshal(messge, &ereMsg)

		assertError(t, ereMsg.Message, "check username and password")
	})
	t.Run("Trying to log in in with empty username", func(t *testing.T) {
		arg := GetAccountParams{
			UserName: "",
			Password: "meku*#76&Ma1Mek9",
		}
		body, err := json.Marshal(arg)
		if err != nil {
			t.Fatal(err)
		}
		req, _ := http.NewRequest(http.MethodGet, "/login", bytes.NewBufferString(string(body)))
		recorder := httptest.NewRecorder()
		server := testServer()
		server.Router.ServeHTTP(recorder, req)
		// t.Fatal(recorder.Body.String())
		var ereMsg errorsMessage
		messge, _ := ioutil.ReadAll(recorder.Body)
		json.Unmarshal(messge, &ereMsg)

		assertError(t, ereMsg.Error, "empty field")
	})
}

func assertError(t testing.TB, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
