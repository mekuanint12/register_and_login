package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	db "github.com/mekuanint12/simple_api/db/sqlc"
	valid "github.com/mekuanint12/simple_api/valid"
	"golang.org/x/crypto/bcrypt"
)

type CreateAccountRequest struct {
	UserName  string `json:"user_name"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Password  string `json:"password"`
	Email     string `json:"email"`
}
type GetAccountParams struct {
	UserName string `json:"user_name"`
	Password string `json:"password"`
}

func (server *Server) createAccount(ctx *gin.Context) {
	var req CreateAccountRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	arg := db.CreateAccountParams{
		UserName:  req.UserName,
		FirstName: req.FirstName,
		LastName:  req.LastName,
		Password:  req.Password,
		Email:     req.Email,
	}

	errUsername := valid.ValidateUsername(arg.UserName)
	if errUsername != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(errUsername))
		return
	}

	// _, e := server.Users.GetOneAccount(ctx, arg.UserName)
	// if e != nil {
	// 	ctx.JSON(http.StatusBadRequest, gin.H{
	// 		"message": "Username Taken",
	// 	})
	// 	return
	// }

	errEmpty := valid.ValidateField(arg.FirstName)
	if errEmpty != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(errEmpty))
		return
	}
	errUsernameL := valid.ValidateField(arg.LastName)
	if errUsernameL != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(errUsernameL))
		return
	}

	errPass := valid.ValidatePassword(arg.Password)
	if errPass != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(errPass))
		return
	}
	errEmail := valid.ValidateEmail(arg.Email)
	if !errEmail {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Email Invalid",
		})
		return
	}
	hashed, _ := bcrypt.GenerateFromPassword([]byte(arg.Password), 8)
	arg.Password = string(hashed)

	_, err := server.Users.CreateAccount(ctx, arg)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Welcome"})
}

func (server *Server) getAccount(ctx *gin.Context) {
	var req GetAccountParams
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}
	arg := GetAccountParams{
		UserName: req.UserName,
		Password: req.Password,
	}
	// errUsernameTaken := valid.UsernameExists(arg.UserName)
	// if errUsernameTaken {
	// 	ctx.JSON(http.StatusBadRequest, gin.H{"message": "check username and password"})
	// 	return
	// }

	errEmpty := valid.ValidateField(arg.UserName)
	if errEmpty != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(errEmpty))
		return
	}
	errEmptyPass := valid.ValidateField(arg.Password)
	if errEmptyPass != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(errEmptyPass))
		return
	}

	acc, err := server.Users.GetOneAccount(ctx, arg.UserName)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "check username and password"})
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(acc.Password), []byte(arg.Password))
	if err == nil {
		ctx.JSON(http.StatusOK, gin.H{"message": "Welcome Back!"})
		return
	}

	ctx.JSON(http.StatusBadRequest, gin.H{"message": "check username and password"})
}

// It starts the HTTP server for us
func (Server *Server) Start(address string) error {
	return Server.Router.Run(address)
}

func errorResponse(err error) gin.H {
	return gin.H{"error": err.Error()}
}
