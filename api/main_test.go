package api

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
	db "github.com/mekuanint12/simple_api/db/sqlc"
)

const (
	DB_DRIVER = "postgres"
	DB_SOURCE = "postgresql://root@:26257/users?application_name=%24+cockroach+sql&connect_timeout=15&sslmode=disable"
)

func testServer() *Server {
	con, err := sql.Open(DB_DRIVER, DB_SOURCE)
	if err != nil {
		log.Fatal("Can not connect to the database:", err)
	}
	create_account := db.NewUser(con)
	server := NewServer(create_account)
	return server
}
