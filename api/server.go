package api

import (
	"github.com/gin-contrib/timeout"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	db "github.com/mekuanint12/simple_api/db/sqlc"
	"time"
)

type Server struct {
	Users  *db.CreateUser
	Router *gin.Engine
}

func NewServer(users *db.CreateUser) *Server {
	server := &Server{Users: users}

	router := gin.New()
	router.POST("/register", timeout.New(timeout.WithTimeout(100*time.Second), timeout.WithHandler(server.createAccount)))
	router.GET("/login", timeout.New(timeout.WithTimeout(100*time.Second), timeout.WithHandler(server.getAccount)))
	server.Router = router
	return server
}
