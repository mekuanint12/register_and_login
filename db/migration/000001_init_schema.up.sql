CREATE TABLE accounts(
  "id" BIGSERIAL PRIMARY KEY,
  "user_name" VARCHAR(10) UNIQUE NOT NULL,
  "first_name" VARCHAR(15) NOT NULL,
  "last_name" VARCHAR(15) NOT NULL,
  "password" VARCHAR(150) NOT NULL,
  "email" VARCHAR(35) UNIQUE NOT NULL,
  "created_on" TIMESTAMPTZ NOT NULL DEFAULT (now())
);
