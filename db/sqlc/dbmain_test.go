package db

import (
	"database/sql"
	"log"
	"os"
	"testing"

	_ "github.com/lib/pq"
	db "github.com/mekuanint12/simple_api/db/sqlc"
	"github.com/mekuanint12/simple_api/util"
)

var testQueries *db.Queries

func TestMain(m *testing.M) {
	config, err := util.LoadConfig("../")
	if err != nil {
		log.Fatal("Cant load config file: ", err)
	}
	conn, er := sql.Open(config.DBDriver, config.DBSource)
	if er != nil {
		log.Fatal("Can not connect to the database:", err)
	}
	testQueries = db.New(conn)
	os.Exit(m.Run())
}
