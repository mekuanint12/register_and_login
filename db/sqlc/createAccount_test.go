package db

import (
	"context"
	"testing"

	db "github.com/mekuanint12/simple_api/db/sqlc"
	"github.com/stretchr/testify/require"
)

func TestCreatingAccount(t *testing.T) {
	arg := db.CreateAccountParams{
		UserName:  "testuser",
		FirstName: "meku",
		LastName:  "last",
		Password:  "*#meku76&12M",
		Email:     "test@test.com",
	}

	account, err := testQueries.CreateAccount(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, account)

	require.Equal(t, arg.UserName, account.UserName)
	require.Equal(t, arg.FirstName, account.FirstName)
	require.Equal(t, arg.LastName, account.LastName)
	require.Equal(t, arg.Email, account.Email)

	require.NotZero(t, account.ID)
	require.NotZero(t, account.CreatedOn)
}
